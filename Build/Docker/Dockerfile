FROM php:7.0-cli

MAINTAINER Marco Huber <marco.huber@bgm-gmbh.de>

LABEL description="Docker image for the Deployment"

ENV DEBIAN_FRONTEND=noninteractive

# Get ruby 1.9.3 from wheeze even if FROM is jessie
RUN echo "deb http://deb.debian.org/debian wheezy main" > /etc/apt/sources.list.d/wheezy.list \
	&& echo "Package: *" > /etc/apt/preferences.d/stable \
	&& echo "Pin: release n=stable" >> /etc/apt/preferences.d/stable \
	&& echo "Pin-Priority: 1000" >> /etc/apt/preferences.d/stable \
	&& echo "Package: ruby*" > /etc/apt/preferences.d/ruby \
	&& echo "Pin: version 1.9.3*" >> /etc/apt/preferences.d/ruby \
	&& echo "Pin-Priority: 1000" >> /etc/apt/preferences.d/ruby

# Install stuff
RUN apt-get update \
	&& apt-get install -y \
		openssh-client \
		git \
		ruby1.9.3 \
		ruby-dev \
		rubygems \
		locales \
	&& rm -rf /var/lib/apt/lists/* \
	&& apt-get autoremove -y \
	&& apt-get clean \
	&& gem install bundler

# Prepare ssh
RUN eval $(ssh-agent -s) \
	&& mkdir -p ~/.ssh \
	&& chmod 700 ~/.ssh \
	&& echo "Host *" > ~/.ssh/config \
	&& echo "StrictHostKeyChecking no" >> ~/.ssh/config \
	&& chmod 600 ~/.ssh/config

# Set locale and timezone
ENV TZ=Europe/Berlin
ENV LANG=en_US.UTF-8
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
	&& echo 'LANG="en_US.UTF-8"'>/etc/default/locale \
	&& dpkg-reconfigure --frontend=noninteractive locales \
	&& update-locale LANG=en_US.UTF-8 \
	&& ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
	&& echo $TZ > /etc/timezone

# Set umask
COPY ./docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]

# Dummy command, overwritten when calling "docker run"
CMD ["echo 'machine running...' && tail -f /dev/null"]
